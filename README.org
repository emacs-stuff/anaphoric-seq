* --seq

Anaphoric forms of [[https://github.com/NicolasPetton/seq.el][seq.el]], like in dash.el.

They all start with a double dash: =seq-map= => =--seq-map=.

Example:

#+BEGIN_SRC emacs-lisp
(--seq-map (* it 2) '(1 2))
;; instead of:
(seq-map (lambda (it) (* it 2)) '(1 2))
#+END_SRC
