
(defmacro --seq-map (form list)
  "Anaphoric form of `seq-map'."
  (declare (debug (form form)))
  `(seq-map (lambda (it) ,form) ,list))

(defmacro --seq-filter (form list)
  "Anaphoric form of `seq-filter'."
  (declare (debug (form form)))
  `(seq-filter (lambda (it) ,form) ,list))

